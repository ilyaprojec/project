  var form = document.getElementById('payment-form');
  var stripe = Stripe('pk_test_51I4h0UDC6PdP7eMd52dqHUejVHj03UnNoNU31ijFmkvlSPPTiPHpVd11dXChbfoEweQGqV05tCQNGTecHf95nbCw00nN7O6BYI');
  var elements = stripe.elements();
  var displayError = document.getElementById('card-errors');

  var style = {
    base: {
        color: "#32325d",
      }
  };

  var card = elements.create("card", { style: style });
  card.mount("#card-element");
  card.on('change', function(event) {
    if (event.error) {

      } else {
        displayError.textContent = '';
      }
   });

  form.addEventListener('submit', function(ev) {
    ev.preventDefault();
    var clientSecret = document.getElementById('card-btn')
    stripe.confirmCardPayment(clientSecret.dataset.secret, {
      payment_method: {
        card: card,
        billing_details: {
          name: document.getElementById('card-btn').dataset.username
        }
      }
    }).then(function(result) {
      if (result.error) {
        displayError.textContent = result.error.message;
      } else {
        // The payment has been processed!
        if (result.paymentIntent.status === 'succeeded') {
            function getCookie(name) {
                let cookieValue = null;
                if (document.cookie && document.cookie !== '') {
                    const cookies = document.cookie.split(';');
                    console.log(cookies);
                    for (let i = 0; i < cookies.length; i++) {
                        const cookie = cookies[i].trim();
                        // Does this cookie string begin with the name we want?
                        if (cookie.substring(0, name.length + 1) === (name + '=')) {
                            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                            break;
                        }
                    }
                }
                return cookieValue;
            }
            const csrftoken = getCookie('csrftoken');
            var formData = new FormData(document.forms.order);

            formData.append("first_name", document.getElementById('card-btn').dataset.username);
            formData.append("csrfmiddlewaretoken", csrftoken)
            var xhr = new XMLHttpRequest();
            xhr.open("POST", "/orders/payed-online-order/");
            xhr.send(formData);
            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4) {
                    location.href="/orders/thank/"
                }
            }
        }
      }
    });
  });
