from django.contrib.auth.admin import UserAdmin
from loginsys.models import MyUser
from django.contrib import admin


class MyUserAdmin(UserAdmin):
    list_display = ('username', 'phone', 'address', 'bonuses')

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'phone', 'password1', 'password2',),
        }),
    )

    fieldsets = (
        (None, {
            'fields': ('username', 'password', 'phone', 'address', 'bonuses')}),
        (('Personal info'), {
            'fields': ('first_name', 'last_name')}),
        (('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        (('Important dates'), {
            'fields': ('last_login', 'date_joined')}),
    )


admin.site.register(MyUser, MyUserAdmin)
