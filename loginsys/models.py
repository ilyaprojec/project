import uuid
from django.core.validators import RegexValidator
from django.db import models
from django.contrib.auth.models import AbstractUser


class MyUser(AbstractUser):
    phone_regex = RegexValidator(
        regex=r'^\+380\d{9}$',
        message="Phone number must be entered in the format: '+380XXXXXXXXX'. 12 digits."
    )

    uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    phone = models.CharField(validators=[phone_regex], max_length=13, blank=True, unique=True)
    address = models.CharField(max_length=255, null=True)
    bonuses = models.DecimalField(max_digits=10, decimal_places=2, default=0)

    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        return str(self.phone)
