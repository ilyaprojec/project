from django.urls import path
from django.conf.urls import url
from loginsys.views import login, logout, RegistrationView, ProfileView, UserUpdateView, profile_change_password, profile_personal_data

urlpatterns = [
    url('login/', login, name='login'),
    url('logout/', logout, name='logout'),
    url('registration/', RegistrationView.as_view(), name='registration'),
    path('update-user/<str:username>/', UserUpdateView.as_view(), name='update-user'),
    url('profile/', ProfileView.as_view(), name='profile'),
    url('change-password/', profile_change_password, name='profile-change-password'),
    url('personal-data/', profile_personal_data, name='profile-personal-data'),
]
