from django.contrib import admin
from foodsys.models import Product, Image, Tag


class ImageInline(admin.TabularInline):
    model = Image
    extra = 0


class ProductAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Product._meta.fields]
    inlines = [ImageInline]

    class Meta:
        model = Product


class ImageAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Image._meta.fields]

    class Meta:
        model = Image


admin.site.register(Product, ProductAdmin)
admin.site.register(Image, ImageAdmin)
admin.site.register(Tag)
