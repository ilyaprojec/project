import re
from django.views.generic.base import TemplateView
from .models import Product, Image


class ProductView(TemplateView):
    template_name = "product.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ProductView, self).get_context_data(*args, **kwargs)
        product_id = re.search(r'\d+$', self.request.path).group()
        context['images'] = Image.objects.all().filter(product__id=product_id)
        context['product'] = Product.objects.get(id=product_id)

        return context


class Menu01View(TemplateView):
    template_name = "menu_list.html"

    def get_context_data(self, *args, **kwargs):
        context = super(Menu01View, self).get_context_data(*args, **kwargs)
        context['products'] = Product.objects.all().filter(tag__name='burger')

        return context


class Menu02View(TemplateView):
    template_name = "menu_list.html"

    def get_context_data(self, *args, **kwargs):
        context = super(Menu02View, self).get_context_data(*args, **kwargs)
        context['products'] = Product.objects.all().filter(tag__name='breakfast&salad')
        return context


class Menu03View(TemplateView):
    template_name = "menu_list.html"

    def get_context_data(self, *args, **kwargs):
        context = super(Menu03View, self).get_context_data(*args, **kwargs)
        context['products'] = Product.objects.all().filter(tag__name='soup')
        return context


class Menu04View(TemplateView):
    template_name = "menu_list.html"

    def get_context_data(self, *args, **kwargs):
        context = super(Menu04View, self).get_context_data(*args, **kwargs)
        context['products'] = Product.objects.all().filter(tag__name='dessert')
        return context
