from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=255, null=True)
    tag = models.ForeignKey("foodsys.Tag", on_delete=models.CASCADE, null=True)
    main_image = models.ImageField(upload_to='static/products', null=True)
    short_desc = models.TextField(max_length=255, null=True)
    consist = models.TextField(max_length=255, null=True)
    price = models.DecimalField(decimal_places=2, max_digits=10, default=0)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return "%s, %s" % (self.price, self.name)


class Tag(models.Model):
    name = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.name


class Image(models.Model):
    product = models.ForeignKey(Product, blank=True, null=True, default=None, on_delete=models.CASCADE)
    tag = models.ForeignKey("foodsys.Tag", on_delete=models.CASCADE, null=True)
    is_active = models.BooleanField(default=True)
    image = models.ImageField(upload_to='static/products', null=True)

    def __str__(self):
        return "%s" % self.id

    class Meta:
        verbose_name = 'Image'
        verbose_name_plural = 'Images'
