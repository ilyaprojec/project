from forms import celery_app
from .models import ProductInCart


@celery_app.task
def clear_cart():
    ProductInCart.objects.filter(user_id__isnull=True).delete()
