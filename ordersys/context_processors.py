from django.contrib.auth import get_user_model
from django.contrib import auth
from .models import ProductInCart


def getting_cart_info(request):
    session_key = request.session.session_key

    if session_key:
        username = auth.get_user(request).username
    else:
        request.session["session_key"] = 123
        request.session.cycle_key()

    if request.user.is_authenticated:
        products_in_cart = ProductInCart.objects.filter(user_id=get_user_model().objects.get(username=request.user.username).id, is_active=True, order__isnull=True)
    else:
        products_in_cart = ProductInCart.objects.filter(session_key=session_key, is_active=True,
                                                        order__isnull=True)
    products_total_nmb = 0

    for product_in_cart in products_in_cart:
        products_total_nmb += product_in_cart.nmb

    return locals()
